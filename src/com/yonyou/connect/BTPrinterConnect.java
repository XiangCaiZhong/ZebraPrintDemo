package com.yonyou.connect;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Looper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.uap.um.base.UMEventArgs;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

public class BTPrinterConnect extends Activity implements DiscoveryHandler{
	public static Activity activity = null;
	public static BTPrinterConnect connect;
	public static ArrayList<HashMap<String,String>> printerList = null;
	public static StringBuffer buffer = null;
	public static boolean isComplement = false;
	public static boolean isConnect = false;
	public static Connection thePrinterConn;
	public static void startDis(UMEventArgs args)throws IOException{
		activity = args.getUMActivity();
		printerList = new ArrayList<HashMap<String,String>>();
		connect = new BTPrinterConnect();
		connect.discoveryBTPirnter();
		return;
	}
	
	public void discoveryBTPirnter(){
		isComplement = false;
		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				try {
					BluetoothDiscoverer.findPrinters(activity,
							BTPrinterConnect.this);
				} catch (ConnectionException e) {
					e.printStackTrace();
				} finally {
					Looper.myLooper().quit();
				}
			}
		}).start();
	}
	
	public static String getStatus(UMEventArgs args)throws IOException{
		if(isComplement){
			return "true";
		}else{
			return "false";
		}
	}
	
	public static String getList(UMEventArgs args)throws IOException{
		buffer = new StringBuffer();
		buffer.append("{\"bluetooth\":[");
		if (printerList.size() > 0) {
			for (HashMap<String,String> device : printerList) {
				buffer.append("{\"bt_name\":\"" + device.get("name") + "\","
						+ "\"bt_addr\":\"" + device.get("addr") + "\","
						+ "\"bt_select\":\"未连接\"},");
			}
			buffer.deleteCharAt(buffer.length() - 1);
		}
		buffer.append("]}");
		return buffer.toString();
	}
	
	public static void getConnect(UMEventArgs args)throws IOException{
		final String addr = args.getString("param");
		new Thread(new Runnable() {
            public void run() {
                try {
                	
                    // Instantiate connection for given Bluetooth&reg; MAC Address.
                    thePrinterConn = new BluetoothConnection(addr);
                    
                    // Initialize
                    Looper.prepare();

                    // Open the connection - physical connection is established here.
                    thePrinterConn.open();
                    
                    isConnect = true;

                    Looper.myLooper().quit();
                } catch (Exception e) {
                    // Handle communications error here.
                    e.printStackTrace();
                }
            }
        }).start();
    }
	
	public static void cancelConnect(UMEventArgs args){
		try {
			if(thePrinterConn.isConnected()){
				thePrinterConn.close();
				isConnect = false;
			}
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getConnectStatus(UMEventArgs args){
		if(isConnect){
			return "connected";
		}else{
			return "unconnected";
		}
	}
	
	public static void send(UMEventArgs args){
		try {
			String label="";
			org.json.JSONObject data = args.getJSONObject("data");
//			String conf_string = data.getString("conf");
//			String code = data.getString("Code");
			
			JSONObject json = JSON.parseObject(data.toString());
			JSONObject conf = json.getJSONObject("conf");
//			String cpclData = lableStart("0",conf.getString("QR_l"),conf.getString("QR_l"),"500","1");
//			cpclData = cpclData + lableWrap();
//			cpclData = cpclData + printQRCode(conf.getString("QR_h"),conf.getString("QR_v"),conf.getString("QR_e"),code);
//			cpclData = cpclData + lableWrap();
//			cpclData = cpclData + printText(conf.getString("T_f"),conf.getString("T_s"),conf.getString("T_h"),conf.getString("T_v"),textGB,textAscii);
//			cpclData = cpclData + lableWrap();
//			cpclData = cpclData + lableEnd();
			int length = json.getString("Code").length();
			String lengthStr = String.valueOf(length);
			int B_length = lengthStr.length();
			switch (B_length) {
			case 0:
				lengthStr = "0000";
				break;
			case 1:
				lengthStr = "000"+lengthStr;
				break;
			case 2:
				lengthStr = "00"+lengthStr;
				break;
			case 3:
				lengthStr = "0"+lengthStr;
				break;
			case 4:
				lengthStr = lengthStr;
				break;
			default:
				break;
			}
			if(json.getString("CodeRule").equals("ML")){
				label = ZPLBQCodeML(conf.getString("QR_l"),lengthStr,conf.getString("QR_h"),conf.getString("QR_v"),conf.getString("QR_e"),conf.getString("T_h"),conf.getString("T_v"),json);
			}else if(json.getString("CodeRule").equals("FM")){
				label = ZPLBQCodeFM(conf.getString("QR_l"),lengthStr,conf.getString("QR_h"),conf.getString("QR_v"),conf.getString("QR_e"),conf.getString("T_h"),conf.getString("T_v"),json);
			}else{
				return;
			}
			//return cpclData;
			//String cpclData1 = "! 0 200 200 500 1\r\nT 4 0 200 100 大西洋\r\nFORM\r\nPRINT\r\n";
//			String cpclData2 = "^XA^CI28^PW384^A@N,24,24,E:HANS.TTF^FO0,10^FD简体中文^FS^XZ";
//			String cpclData1 = "! 0 200 200 225 1\r\nPW 384\r\nENCODING GB18030\r\nT GBUNSG24.CPF 0 200 350 用友"+"\r\nFORM\r\nPRINT\r\n";
//			if(thePrinterConn==null){
//				return;
//			}
			//String confSet = "^XA^MNN,0^XZ";
			thePrinterConn.write(label.getBytes());
			//thePrinterConn.write(confSet.getBytes());
			//thePrinterConn.write(cpclData.getBytes());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			//return null;
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 字符串转unicode
	 * @param msg
	 * @return
	 */
	public static String string2Unicode(String msg){
		String s = "";
		int chr1;
		for(int i = 0;i<msg.length();i++){
			chr1 = (char)msg.charAt(i);
			if(chr1>=19968&&chr1<=171941){
				s+="\\u" + Integer.toHexString(chr1);
			}else{
				s+="\\u00" + Integer.toHexString(chr1);
			}
		}
		return s;
	}
	
	/**
	 * unicode 转字符串
	 */
	public static String unicode2String(String unicode) {
	 
	    StringBuffer string = new StringBuffer();
	 
	    String[] hex = unicode.split("\\\\u");
	 
	    for (int i = 1; i < hex.length; i++) {
	 
	        // 转换出每一个代码点
	        int data = Integer.parseInt(hex[i], 16);
	 
	        // 追加成string
	        string.append((char) data);
	    }
	 
	    return string.toString();
	}
	
	@Override
	public void discoveryError(String arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void discoveryFinished() {
		// TODO Auto-generated method stub
		isComplement = true;
	}

	@Override
	public void foundPrinter(DiscoveredPrinter printer) {
		// TODO Auto-generated method stub
		HashMap<String,String> map = new HashMap<String,String>();
		int i = printerList.size()+1;
		map.put("name","斑马打印机"+i);
		map.put("addr",printer.address);
		printerList.add(map);
	}
	
	/**
	 * ZPL标签
	 * @param HorizontalBQ 二维码水平位置
	 * @param VerticalBQ   二维码垂直位置
	 * @param mag          二维码放大倍数
	 * @param msg          二维码信息
	 * @param HorizontalTX 文本水平位置
	 * @param VerticalTX   文本垂直位置
	 * @param TX           文本信息
	 * @return
	 */
    public static String ZPLBQCodeML(String printLong,String lengthStr,String HorizontalBQ,String VerticalBQ,String mag,String HorizontalTX,String VerticalTX,JSONObject json){
        String lable="^XA"
        		+"^MNN,0^LL"+printLong
        		+"^FO"+HorizontalBQ+","+VerticalBQ+"^BQN,2,"+mag+"^FDMM,B"+lengthStr+json.getString("Code")+"^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+VerticalTX+"^FD物料^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(30+Integer.parseInt(VerticalTX))+"^FD规格^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(60+Integer.parseInt(VerticalTX))+"^FD型号^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(90+Integer.parseInt(VerticalTX))+"^FD批次号^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(120+Integer.parseInt(VerticalTX))+"^FD卷号^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(150+Integer.parseInt(VerticalTX))+"^FD重量^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+(100+Integer.parseInt(HorizontalTX))+","+VerticalTX+"^FD"+json.getString("name")+"^FS"
        		+"^FO"+(100+Integer.parseInt(HorizontalTX))+","+(30+Integer.parseInt(VerticalTX))+"^A0N,20,20^FD"+json.getString("spec")+"^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+(100+Integer.parseInt(HorizontalTX))+","+(60+Integer.parseInt(VerticalTX))+"^FD"+json.getString("type")+"^FS"
                +"^FO"+(100+Integer.parseInt(HorizontalTX))+","+(90+Integer.parseInt(VerticalTX))+"^A0N,20,20^FD"+json.getString("vbillcode")+"^FS"
                +"^FO"+(100+Integer.parseInt(HorizontalTX))+","+(120+Integer.parseInt(VerticalTX))+"^A0N,20,20^FD"+json.getString("jh")+"^FS"
                +"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+(100+Integer.parseInt(HorizontalTX))+","+(150+Integer.parseInt(VerticalTX))+"^FD"+json.getString("weight")+"^FS"
        		+"^XZ";
        return lable;
    }
    
    public static String ZPLBQCodeFM(String printLong,String lengthStr,String HorizontalBQ,String VerticalBQ,String mag,String HorizontalTX,String VerticalTX,JSONObject json){
        String lable="^XA"
        		+"^MNN,0^LL"+printLong
        		+"^FO"+HorizontalBQ+","+VerticalBQ+"^BQN,2,"+mag+"^FDMM,B"+lengthStr+json.getString("Code")+"^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+VerticalTX+"^FD物料^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(30+Integer.parseInt(VerticalTX))+"^FD型号^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+HorizontalTX+","+(60+Integer.parseInt(VerticalTX))+"^FD批次号^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+(100+Integer.parseInt(HorizontalTX))+","+VerticalTX+"^FD"+json.getString("name")+"^FS"
        		+"^CI28^PW384^A@N,24,24,E:HANS.TTF^FO"+(100+Integer.parseInt(HorizontalTX))+","+(30+Integer.parseInt(VerticalTX))+"^FD"+json.getString("type")+"^FS"
                +"^FO"+(100+Integer.parseInt(HorizontalTX))+","+(60+Integer.parseInt(VerticalTX))+"^A0N,20,20^FD"+json.getString("vbillcode")+"^FS"
        		+"^XZ";
        return lable;
    }
	
	/**
	 * 标签开始
     * @param offset        偏移
     * @param Horizontal    水平尺寸
     * @param Vertical      垂直尺寸
     * @param height        高度
     * @param qty           数量
     * @return
     */
    public static String lableStart(String offset,String Horizontal,String Vertical,String height,String qty){
        String strLable="!"+" "+offset+" "+Horizontal+" "+
                Vertical+" "+height+" "+qty;

        return strLable;
    }
    
    /**
     * 打印文本
     * @param font          字体
     * @param size          字号
     * @param x             起始水平坐标
     * @param y             起始垂直坐标
     * @param strValue      内容
     * @return
     */
    public static String printText(String font, String size, String x, String y,String strValueGB,String strValueAscii) {
        int y_d = Integer.parseInt(y) +100;
    	String strText = "PW 384\r\n"+"ENCODING GB18030\r\n"+"T" + " " + "GBUNSG24.CPF" + " "
                + size + " " + x + " "
                + y + " " + strValueGB+ "\r\n" + "ENCODING ASCII\r\n" + "T" + " " + font + " "
                + size + " " + x + " "
                + y_d + " " + strValueAscii;

        return strText;
    }
    
    /**
     * 打印二维码
     * @param x             起始水平坐标
     * @param y             起始垂直坐标
     * @param strValue      内容
     * @return
     */
    public static String printQRCode(String x,String y,String e,String strValue){
        /**
         * M: QR code model No.Range is 1 or 2.Default is 2.
         * U: Unit-width/Unit-height of the module.Range is 1 to 32.Default is 6.
         * "M" is the error correction parameter (L=Low, M=Medium, Q=Medium High, H=High)
         * "0"  is the mask pattern
         * "A" is the mode conversion (A=Auto, M=Manual)
         */

        String strWrap="\r\n";
        String strBarCode = "B" + " QR " + x + " " + y + " M 2 "+ " U "+ e +strWrap+"MA,"+ strValue + strWrap+"ENDQR";

        return strBarCode;
    }
    
    public static String lableWrap(){
    	String strWrap="\r\n";
        
    	return strWrap;
    }
    
    /**
     * 标签结束
     * @return
     */
    public static String lableEnd(){
    	String strWrap="\r\n";
    	String strLable="FORM"+strWrap+"PRINT"+strWrap;
        
    	return strLable;
    }
}
